<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShowProfile;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('user/{id}', ShowProfile::class);
Route::get('profile', [UserController::class, 'show'])->middleware('auth');

Route::resource('photos', PhotoController::class);

Route::resources([
    'photos' => PhotoController::class,
    'posts' => PostController::class,
]);
