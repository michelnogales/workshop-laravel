<?php namespace App\Exports;


use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;



class UsersFullExport implements
    FromCollection,
    ShouldAutoSize,
    WithHeadings,
    WithTitle,
    WithEvents,
    WithCustomStartCell
{

    private $lastRow;
    private static $ALIGNMENT = '\\PhpOffice\\PhpSpreadsheet\\Style\\Alignment';
    private static $FILL      = '\\PhpOffice\\PhpSpreadsheet\\Style\\Fill';
    private static $BORDER 	  = '\\PhpOffice\\PhpSpreadsheet\\Style\\Border';

    public function headings(): array
    {
        return [
            'Nombre',
            'Correo'
        ];
    }

    public function startCell(): string
    {
        return 'A1';
    }

    public function title(): string
    {
        return 'Usuarios';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:B1',
                    [
                        'font' => [
                            'bold' => true,
                            'name' =>  'Montserrat',
                            'size' =>  14
                        ],
                        'alignment' => [
                            'horizontal' => static::$ALIGNMENT::HORIZONTAL_CENTER,
                            'vertical' => static::$ALIGNMENT::VERTICAL_CENTER,
                        ],
                        'fill' => [
                            'fillType' => static::$FILL::FILL_SOLID,
                            'startColor' => [
                                'argb' => 'FFFFFFFF',
                            ],
                        ]
                    ]
                );



                $event->sheet->styleCells(
                    'A1:B'.$this->lastRow,
                    [
                        'font' => [
                            'name' =>  'Montserrat',
                            'size' =>  12
                        ],
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => static::$BORDER::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ]
                        ]
                    ]
                );

                $event->sheet->autoFilter('A1:B1');
                $event->sheet->wrapText('A1:B1');
                $event->sheet->rowHeight('1', 40);
            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {


            $registers  = collect();
            $collection = collect();

            User::select('name', 'email')
            ->orderBy('id', 'DESC')
            ->chunk(1000, function (Collection $rows) use (&$registers) {
                $registers = $registers->merge($rows);
            });

            $row = 1;
            foreach ($registers as $register) {
                $collection->push(
                    [
                        $register->name,
                        $register->email
                    ]
                );
                $row++;
            }
            $this->lastRow = $row;

            return $collection;


    }
}
